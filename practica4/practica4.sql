﻿-- Consultas. Práctica 4.

-- Ejercicio 1. Averigua el DNI de todos los clientes.

SELECT dni FROM cliente;

-- Ejercicio 2. Consulta todos los datos de todos los programas.

SELECT * FROM programa;

-- Ejercicio 3. Obtén un listado con los nombres de todos los programas.

SELECT DISTINCT nombre FROM programa;

-- Ejercicio 4. Genera una lista con todos los comercios.

SELECT cif,nombre,ciudad FROM comercio;

-- Ejercicio 5. Genera una lista de las ciudades con establecimientos donde se venden programas, sin que aparezcan valores duplicados
-- (utiliza DISTINCT).

SELECT DISTINCT nombre, ciudad FROM comercio;

-- Ejercicio 6. Obtén una lista con los nombres de programas, sin que aparezcan valores duplicados (utiliza DISTINCT).

SELECT DISTINCT nombre FROM programa;

-- Ejercicio 7. Obtén el DNI más 4 de todos los clientes.

SELECT (dni+4) dni FROM cliente;

-- Ejercicio 8. Haz un listado con los códigos de los programas multiplicados por 7.

SELECT (codigo*7) codigo FROM programa;

-- Ejercicio 9. ¿Cuáles son los programas cuyo código es inferior o igual a 10?

SELECT DISTINCT codigo FROM programa WHERE codigo<=10;

-- Ejercicio 10. ¿Cuál es el programa cuyo código es 11?

SELECT nombre FROM programa WHERE codigo=11;

-- Ejercicio 11. ¿Qué fabricantes son de Estados Unidos?

SELECT nombre FROM fabricante WHERE pais='Estados Unidos';

-- Ejercicio 12. ¿Cuáles son los fabricantes no españoles? Utilizar el operador IN.

SELECT DISTINCT nombre FROM fabricante WHERE pais NOT IN ('España');

-- Ejercicio 13. Obtén un listado con los códigos de las distintas versiones de Windows.

SELECT version FROM programa  WHERE nombre='WINDOWS';

-- Ejercicio 14. ¿En qué ciudades comercializa programas El Corte Inglés?

SELECT f.pais FROM fabricante f WHERE f.nombre='Corte Ingles';

-- Ejercicio 15. ¿Qué otros comercios hay, además de El Corte Inglés? Utilizar el operador IN.

SELECT nombre FROM comercio  WHERE nombre NOT IN ('El Corte Inglés');

-- Ejercicio 16. Genera una lista con los códigos de las distintas versiones de Windows y Access. Utilizar el operador IN.

SELECT codigo FROM programa WHERE  nombre= 'WINDOWS' AND nombre = 'Access';

-- Ejercicio 17. Obtén un listado que incluya los nombres de los clientes de edades comprendidas entre 10 y 25 y de los mayores de 50 años.
-- Da una solución con BETWEEN y otra sin BETWEEN.

SELECT nombre FROM cliente WHERE  edad BETWEEN 10 AND 25 OR edad>50;

-- Ejercicio 18. Saca un listado con los comercios de Sevilla y Madrid. No se admiten valores duplicados.

SELECT DISTINCT c.nombre FROM comercio c WHERE c.ciudad='Sevilla' OR c.ciudad='Madrid';

-- Ejercicio 19. ¿Qué clientes terminan su nombre en la letra “o”?

SELECT c.nombre FROM cliente c WHERE c.nombre LIKE  '%o';

-- Ejercicio 20. ¿Qué clientes terminan su nombre en la letra “o” y, además, son mayores de 30 años?

SELECT c.nombre FROM cliente c WHERE c.nombre LIKE  '%o' AND c.edad>30;

-- Ejercicio 21. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A
-- o por una W.

SELECT p.nombre FROM programa p WHERE p.version LIKE '%i' OR p.nombre LIKE 'A%' OR p.nombre LIKE 'W%';

-- Ejercicio 22. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A
-- y termine por una S.

SELECT p.version, p.nombre  FROM programa p WHERE p.version LIKE '%i'  OR p.nombre LIKE 'A%' AND p.nombre LIKE '%S';

-- Ejercicio 23. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, y cuyo nombre no comience por una
-- A.

SELECT p.version, p.nombre  FROM programa p  WHERE p.version LIKE '%i'  AND p.nombre NOT LIKE 'A%';

-- Ejercicio 24. Obtén una lista de empresas por orden alfabético ascendente.

SELECT DISTINCT f.nombre FROM fabricante f  ORDER BY f.nombre ASC;

-- Ejercicio 25. Genera un listado de empresas por orden alfabético descendente.

SELECT DISTINCT f.nombre FROM fabricante f ORDER BY f.nombre DESC;

-- Ejercicio 26. Obtén un listado de programas por orden de versión.

SELECT DISTINCT p.version  FROM programa p  ORDER BY p.version ASC;

-- Ejercicio 27. Genera un listado de los programas que desarrolla Oracle.

-- C1 
  SELECT f.id_fab  FROM fabricante f  WHERE f.nombre='Oracle';

  -- Consulta final
  SELECT DISTINCT p.nombre  FROM desarrolla d 
    JOIN programa p 
    ON d.codigo = p.codigo 
    WHERE d.id_fab=(
      SELECT f.id_fab 
        FROM fabricante f 
        WHERE f.nombre='Oracle');

-- Ejercicio 28. ¿Qué comercios distribuyen Windows?

-- C1 
  SELECT p.codigo 
    FROM programa p 
    WHERE p.nombre = 'Windows';

  -- Consulta final
  SELECT c.nombre   FROM distribuye d 
    JOIN comercio c 
    ON d.cif = c.cif 
    WHERE d.codigo IN (
      SELECT p.codigo 
        FROM programa p 
        WHERE p.nombre = 'Windows');

-- Ejercicio 29. Genera un listado de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid.

-- C1 - 
  SELECT c.cif  FROM comercio c  WHERE c.nombre = 'El Corte Ingles'  AND c.ciudad = 'Madrid';

  -- Consulta final
  SELECT p.nombre, p.version, d.cantidad   FROM programa p 
   JOIN distribuye d 
    JOIN (
      SELECT c.cif 
        FROM comercio c 
        WHERE c.nombre = 'El Corte Ingles' 
        AND c.ciudad = 'Madrid'
      )c1 
      ON p.codigo = d.codigo 
      AND d.cif = c1.cif;

-- Ejercicio 30. ¿Qué fabricante ha desarrollado Freddy Hardest?

  -- C1 
  SELECT p.codigo  FROM programa p   WHERE p.nombre ='Freddy Hardest';

  -- Consulta final
  SELECT f.nombre  FROM fabricante f 
    JOIN desarrolla d 
    JOIN (  SELECT p.codigo  FROM programa p  WHERE p.nombre ='Freddy Hardest' )c1 
    ON f.id_fab = d.id_fab 
    AND d.codigo = c1.codigo;


-- Ejercicio 31. Selecciona el nombre de los programas que se registran por Internet.

SELECT p.nombre  FROM registra r  JOIN programa p  ON r.codigo = p.codigo  WHERE r.medio = 'Internet';

-- Ejercicio 32. Selecciona el nombre de las personas que se registran por Internet.

SELECT c.nombre FROM registra r  JOIN cliente c  ON r.dni = c.dni WHERE r.medio = 'Internet';

-- Ejercicio 33. ¿Qué medios ha utilizado para registrarse Pepe Pérez?

-- C1 
  SELECT c.dni  FROM cliente c WHERE c.nombre = 'Pepe Pérez';

  -- Consulta final
  SELECT r.medio  FROM registra r 
    JOIN (
      SELECT c.dni 
        FROM cliente c 
        WHERE c.nombre = 'Pepe Pérez'
    )c1 
    ON r.dni = c1.dni;

-- Ejercicio 34. ¿Qué usuarios han optado por Internet como medio de registro?

SELECT c.nombre  FROM registra r  JOIN cliente c  ON r.dni = c.dni  WHERE r.medio = 'Internet';

-- Ejercicio 35. ¿Qué programas han recibido registros por tarjeta postal?

SELECT p.nombre, p.version  FROM registra r  JOIN programa p  ON r.codigo = p.codigo  WHERE r.medio='Tarjeta Postal';

-- Ejercicio 36. ¿En qué localidades se han vendido productos que se han registrado por Internet?

SELECT c.ciudad  FROM registra r JOIN comercio c  ON r.cif = c.cif  WHERE r.medio = 'Internet';

-- Ejercicio 37. Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para
-- los que ha efectuado el registro.

SELECT c.nombre AS cliente, p.nombre, p.version 
    FROM cliente c 
    JOIN registra r 
    JOIN programa p 
    ON c.dni = r.cif 
    AND r.codigo = p.codigo 
    WHERE r.medio='Internet';

-- Ejercicio 38. Genera un listado en el que aparezca cada cliente junto al programa que ha registrado, el medio con el que lo ha hecho y el
-- comercio en el que lo ha adquirido.

SELECT DISTINCT c.nombre, c.dni 
    FROM cliente c 
    JOIN registra r 
    ON c.dni = r.dni;

SELECT c1.nombre cliente, p.nombre, p.version, p.codigo 
    FROM programa p 
    JOIN registra r 
    JOIN (
      SELECT DISTINCT c.nombre, c.dni 
        FROM cliente c 
        JOIN registra r 
        ON c.dni = r.dni
      )c1 
    ON p.codigo = r.codigo 
    AND c1.dni = r.dni;

-- consulta final 
  SELECT c2.cliente, c2.nombre, c2.version, c.nombre comercio, r.medio 
    FROM comercio c 
    JOIN registra r 
    JOIN (
      SELECT c1.nombre cliente, p.nombre, p.version, p.codigo 
        FROM programa p 
        JOIN registra r 
        JOIN (
          SELECT DISTINCT c.nombre, c.dni 
            FROM cliente c 
            JOIN registra r 
            ON c.dni = r.dni
          )c1 
        ON p.codigo = r.codigo 
        AND c1.dni = r.dni
      )c2 
    ON c.cif = r.cif 
    AND r.codigo = c2.codigo;



-- Ejercicio 39. Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle.

-- C1 
  SELECT f.id_fab 
    FROM fabricante f 
    WHERE f.nombre='Oracle';

  -- C2 
  SELECT p.codigo 
    FROM desarrolla d 
    JOIN programa p 
    ON d.codigo = p.codigo 
    WHERE d.id_fab=(
      SELECT f.id_fab 
        FROM fabricante f 
        WHERE f.nombre='Oracle');

  -- Consulta final
  SELECT DISTINCT c.ciudad 
    FROM programa p 
    JOIN distribuye d 
    JOIN comercio c 
    ON p.codigo = d.codigo 
    AND d.cif = c.cif 
    WHERE p.codigo 
    IN (
      SELECT p.codigo 
        FROM desarrolla d 
        JOIN programa p 
        ON d.codigo = p.codigo 
        WHERE d.id_fab=(
          SELECT f.id_fab 
            FROM fabricante f 
            WHERE f.nombre='Oracle'));

-- Ejercicio 40. Obtén el nombre de los usuarios que han registrado Access XP.

SELECT c.nombre  FROM programa p   JOIN registra r   JOIN cliente c   ON p.codigo = r.codigo   AND r.dni = c.dni   WHERE p.nombre='Access'   AND p.version='XP';

-- Ejercicio 41.  Nombre de aquellos fabricantes cuyo país es el mismo que ʻOracleʼ. (Subconsulta).

-- C1
  SELECT f.pais 
    FROM fabricante f 
    WHERE f.nombre='Oracle';

  -- Consulta final
  SELECT f.nombre 
    FROM fabricante f 
    WHERE f.pais=(
      SELECT f.pais 
        FROM fabricante f 
        WHERE f.nombre='Oracle');

-- Ejercicio 42. Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez. (Subconsulta).

-- C1 - 
  SELECT c.edad
    FROM cliente c
    WHERE c.nombre='Pepe Pérez';

  -- Consulta final
  SELECT c.nombre 
    FROM cliente c 
    WHERE c.edad = (
      SELECT c.edad
        FROM cliente c
        WHERE c.nombre='Pepe Pérez');

-- Ejercicio 43. Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio ʻFNACʼ. (Subconsulta).

-- C1 - 
  SELECT c.ciudad 
    FROM comercio c 
    WHERE c.nombre='FNAC';

  -- Consulta final
  SELECT c.nombre 
    FROM comercio c 
    WHERE c.ciudad=(
      SELECT c.ciudad 
        FROM comercio c 
        WHERE c.nombre='FNAC');

-- Ejercicio 44. Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente ʻPepe Pérezʼ. (Subconsulta).

-- C1 
  SELECT c.dni 
    FROM cliente c 
    WHERE c.nombre = 'Pepe Pérez';

  -- C2 - 
  SELECT DISTINCT r.medio 
    FROM cliente c 
    JOIN registra r
    ON c.dni = r.dni 
    WHERE r.dni = (
      SELECT c.dni 
        FROM cliente c 
        WHERE c.nombre = 'Pepe Pérez');

  -- Consulta final
  SELECT DISTINCT c.nombre 
    FROM cliente c 
    JOIN registra r 
    ON c.dni = r.dni 
    WHERE r.medio 
    IN (
      SELECT DISTINCT r.medio 
        FROM cliente c 
        JOIN registra r
        ON c.dni = r.dni 
        WHERE r.dni = (
          SELECT c.dni 
            FROM cliente c 
            WHERE c.nombre = 'Pepe Pérez'));

-- Ejercicio 45. Obtener el número de programas que hay en la tabla programas.

SELECT COUNT(*)programas  FROM programa p;

-- Ejercicio 46. Calcula el número de clientes cuya edad es mayor de 40 años.

SELECT COUNT(*)clientes   FROM cliente c   WHERE c.edad>40;

-- Ejercicio 47. Calcula el número de productos que ha vendido el establecimiento cuyo CIF es 1.

SELECT SUM(d.cantidad)vendidos  FROM distribuye d  WHERE d.cif=1;

-- Ejercicio 48. Calcula la media de programas que se venden cuyo código es 7.

SELECT AVG(d.cantidad)media FROM distribuye d  WHERE d.codigo=7;

-- Ejercicio 49. Calcula la mínima cantidad de programas de código 7 que se ha vendido.

SELECT MIN(d.cantidad)minimo  FROM distribuye d  WHERE d.codigo=7;

-- Ejercicio 50. Calcula la máxima cantidad de programas de código 7 que se ha vendido.

SELECT MAX(d.cantidad)maximo   FROM distribuye d   WHERE d.codigo=7;

-- Ejercicio 51. ¿En cuántos establecimientos se vende el programa cuyo código es 7?

SELECT COUNT(*)establecimientos  FROM distribuye d  JOIN comercio c    ON d.cif = c.cif   WHERE d.codigo=7;

-- Ejercicio 52. Calcular el número de registros que se han realizado por Internet.

SELECT COUNT(*)registrosInternet  FROM registra r WHERE r.medio = 'Internet';

-- Ejercicio 53. Obtener el número total de programas que se han vendido en ʻSevillaʼ.

SELECT SUM(d.cantidad)ventasSevilla   FROM distribuye d   JOIN comercio c   ON d.cif = c.cif   WHERE c.ciudad = 'Sevilla';

-- Ejercicio 54. Calcular el número total de programas que han desarrollado los fabricantes cuyo país es ʻEstados Unidosʼ.

SELECT COUNT(*)programas  FROM fabricante f  JOIN desarrolla d  ON f.id_fab = d.id_fab  WHERE f.pais='Estados Unidos'; 

-- Ejercicio 55. Visualiza el nombre de todos los clientes en mayúscula. En el resultado de la consulta debe aparecer también la longitud de
-- la cadena nombre.

SELECT UPPER(c.nombre)nombre, CHAR_LENGTH(c.nombre)caracteres   FROM cliente c;

-- Ejercicio 56. Con una consulta concatena los campos nombre y versión de la tabla PROGRAMA.

SELECT CONCAT_WS(' ',p.nombre, p.version)programa  FROM programa p;




















