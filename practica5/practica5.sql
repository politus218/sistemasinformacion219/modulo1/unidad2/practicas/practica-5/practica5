﻿-- Consultas practica 5.

USE practica5;

-- Ejercicio 1. Mostrar el nombrefe cuyo cargo sea presidente. 
-- c1
SELECT c.nombrefe FROM composicion c WHERE c.cargo='presidente';
-- final
SELECT m.nombre_m FROM miembro m JOIN ( SELECT c.nombrefe FROM composicion c WHERE c.cargo='presidente')c1;

-- Ejercicio 2. Mostrar el nombrefe cuyo cargo sea gerente.
-- c1 
SELECT  c.nombrefe FROM  composicion c WHERE c.cargo='gerente';
-- final
SELECT f.direccion FROM federacion f JOIN ( SELECT  c.nombrefe FROM  composicion c WHERE c.cargo='gerente')c1;

-- Ejercicio 3. Mostrar el nombre cuyo cargo sea asesor técnico
--  c1
SELECT c.nombrefe FROM composicion c WHERE c.cargo='asesor tecnico';
--  final
SELECT f.nombre FROM federacion f LEFT JOIN ( SELECT c.nombrefe FROM composicion c WHERE c.cargo='asesor tecnico')c1 ON f.nombre=c1.nombrefe;

